using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using TMPro;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using System;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using UnityEngine.Serialization;


public class ShowText : MonoBehaviour
{
    public                                  GameObject                       NumberBinBags;
    [FormerlySerializedAs("script")] public GeneratorScriptWithKeyboardInput scriptWithKeyboardInput;
    public                                  TextMeshPro                      PreviewText;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        scriptWithKeyboardInput = NumberBinBags.GetComponent<GeneratorScriptWithKeyboardInput>();
        PreviewText = this.GetComponent<TextMeshPro>();
        string number = scriptWithKeyboardInput.number.ToString();
        PreviewText.text = number;
    }
}