using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using System;

public class VirtualKeyboard : MonoBehaviour

{
    public TouchScreenKeyboard keyboard;
    public string              keyboardText;
    public short               selected_number;


    public void OpenSystemKeyboard()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false);
    }

    // Start is called before the first frame update
    void Start()
    {
        // ShowKeyboard(text = "", multiLine = false);
    }

    // Update is called once per frame
    void Update()
    {
        if (keyboard != null)
        {
            keyboardText = keyboard.text;
            // Do stuff with keyboardText
        }
    }

    public void keyboardInt()
    {
        selected_number = Int16.Parse(keyboardText);
    }
}