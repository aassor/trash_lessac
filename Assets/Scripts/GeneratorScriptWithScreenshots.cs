using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Serialization;
using UnityEngine.Windows.WebCam;

public class GeneratorScriptWithScreenshots : MonoBehaviour
{
    public  int                         instanceCount;
    public  List<GameObject>            binbags;
    public  GameObject                  proto;
    private Transform                   _globalScaler;
    public  MixedRealityKeyboardPreview keyboard;
    public  GameObject                  physical_keyboard;
    public  float                       height;
    public  bool                        generated           = false;
    public  PhotoCamera                 photoCamera         = null;
    public  GameObject                  debugWindow         = null;
    public  TextMesh                    debugWindowTextMesh = null;
    public  TextMesh                    countTextMesh       = null;

    public GameObject mainMenu = null;

    public int number;


    //public float position_z;

    public float position_z = 3;

    public float position_x = 0;

    public ResetScript myResetScript = null;


    // Start is called before the first frame update
    void Start()
    {
        binbags = new List<GameObject>();
    }

    public void LaunchPhotoScript()
    {
        myResetScript.ResetTrashBags();
        Debug.Log("Enter script, Try to launch coroutine");
        StartCoroutine(LaunchScript());
    }

    private IEnumerator LaunchScript()
    {
        Debug.Log("Enter coroutines is success");
        mainMenu.SetActive(false);
        debugWindow.SetActive(true);
        countTextMesh.text = "";
        debugWindowTextMesh.text = "Photo script starting in";

        // Giving time to experimenter to set the HoloLens
        for (int time = 5; time > 0; time--)
        {
            debugWindowTextMesh.text = time.ToString();

            yield return new WaitForSeconds(1);
        }

        debugWindow.SetActive(false);

        binbags = new List<GameObject>();
        GenerateTrash(
            position_z,
            position_x);
    }

    IEnumerator Generator(int instanceCount, float position_z, float position_x)
    {
        Debug.Log("Enter generator function");
        _globalScaler = proto.GetComponent<Transform>();
        Rigidbody rigidB = proto.GetComponent<Rigidbody>();

        if (instanceCount == 0)
        {
            Debug.Log("pas de sacs");
            yield return new WaitForSeconds(1);
        }
        else
        {
            for (var i = 0; i < instanceCount; i++)
            {
                string photoName = "TrashBag" + (i + 1) + ".jpg";

                GenerateOneBag(i);

                countTextMesh.text = (i + 1).ToString();

                yield return new WaitForSeconds(1); // Wait for the bag to hit the floor

                photoCamera.MakeAPhoto(photoName);


                if (!photoCamera.windowsCaptionProcess.PhotoCaptureObjectExists())
                {
                    while (!photoCamera.windowsCaptionProcess.PhotoCaptureObjectExists())
                    {
                        yield return new WaitForSeconds(1);
                    }
                }

                while (photoCamera.IsPhotoProcessActivated())
                {
                    yield return new WaitForSeconds(1);
                }
            }
        }

        countTextMesh.text = "Counter enable";
        mainMenu.SetActive(true);
    }

    public void GenerateTrash(float position_z, float position_x)

    {
        int instanceCount = 0;
        bool tryingParsing = int.TryParse(keyboard.GetComponent<ReadInput>().input, out instanceCount);

        if (tryingParsing)
        {
            position_x = this.transform.position.x;
            position_z = this.transform.position.z;
            StartCoroutine(Generator(instanceCount, position_z, position_x));
            generated = true;
        }
        else
        {
            Debug.Log("Wrong string : conversion into int failed");
            mainMenu.SetActive(true);
        }
    }

    private void GenerateOneBag(int currentBagNb)
    {
        Vector3 popos = new Vector3(position_x, height, position_z);
        GameObject t = Instantiate(proto, popos, UnityEngine.Random.rotation);
        Debug.Log(currentBagNb + "eme bag instantiate");


        Transform t_t = t.transform;
        t_t.localScale = _globalScaler.transform.localScale;


        t.name = "bag" + currentBagNb;


        t.SetActive(true);

        binbags.Add(t);
    }
}