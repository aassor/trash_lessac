using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPosition : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject camera;

    void Start()
    {
    }

    // Cette fonction est appel�e lors du d�clenchemment de l'affichage de tas de poubelle, pour localise celui ci � 3 m�tres devant la position de l'utilisateur.
    public void applyMenuposition()
    {
        transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y,
                                         camera.transform.position.z + 3f);
    }
}