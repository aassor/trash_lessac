using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHideAllBags : MonoBehaviour
{
    // Start is called before the first frame update
    public int        number_of_bags;
    public bool       active = true;
    public bool       stored = false;
    public GameObject bags;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void disableTrashBags()
    {
        number_of_bags = this.GetComponent<GeneratorScriptWithKeyboardInput>().instanceCount;
        if (active)
        {
            for (int i = 0; i < number_of_bags; i++)
            {
                if (!stored)
                {
                    GameObject.Find("bag" + i).transform.parent = bags.transform;
                }
            }

            stored = true;
            bags.SetActive(false);
            //GameObject.Find("bag" + i).SetActive(false);
            active = false;
        }
    }

    public void enableTrashBags()
    {
        if (!active)
        {
            for (int i = 0; i < number_of_bags; i++)
            {
                bags.SetActive(true);
                //GameObject[] allObjects = Resources.FindObjectsOfTypeAll<GameObject>();
                //GameObject bag= allObjects.Find("bag" + i).SetActive(true);
                //GameObject.Find("bag" + i).SetActive(true);
            }

            active = true;
        }
    }
}