using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyGreenColor : MonoBehaviour
{
    // Start is called before the first frame update
    public int              number;
    public List<GameObject> bags;
    public Material         mat;

    void Start()
    {
        GeneratorScript script = FindObjectOfType<GeneratorScript>();
        bags = script.binbags;
        int start = bags.Count - number;
        for (int i = start; i < bags.Count; i++)
        {
            bags[i].transform.Find("binbag2").GetComponent<Renderer>().material = mat;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void colorbags(int number)
    {
        GeneratorScript script = FindObjectOfType<GeneratorScript>();
        bags = script.binbags;
        int start = bags.Count - number;
        for (int i = start; i < bags.Count; i++)
        {
            bags[i].transform.Find("binbag2").GetComponent<Renderer>().material = mat;
        }
    }
}