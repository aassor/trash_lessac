using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using TMPro;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using System;
using UnityEngine.UI;

public class ReadPhysicalInput : MonoBehaviour

{
    public string string_number = "";

    public MixedRealityKeyboardPreview keyboard_txt;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        GetNumberString();
        keyboard_txt.PreviewText.text = string_number;
    }


    public void GetNumberString()
    {
        keyboard_txt.enabled = true;

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            string_number = string_number + ("0");
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            string_number = string_number + ("1");
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            string_number = string_number + ("2");
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            string_number = string_number + ("3");
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            string_number = string_number + ("4");
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            string_number = string_number + ("5");
        }

        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            string_number = string_number + ("6");
        }

        if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            string_number = string_number + ("7");
        }

        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            string_number = string_number + ("8");
        }

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            string_number = string_number + ("9");
        }
    }
}