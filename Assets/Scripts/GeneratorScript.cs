using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Microsoft.MixedReality.Toolkit.Experimental.UI;

public class GeneratorScript : MonoBehaviour
{
    //public Transform prefab;
    public int                  instanceCount;
    public List<GameObject>     binbags;
    public GameObject           binPrefab;
    public MixedRealityKeyboard keyboard;
    public float                height;
    public bool                 generated = false;
    public int                  number;

    public float position_z;
    public float position_x;
    public float defaultSpawnFrequency = 1;

    public ResetScript myResetScript = null;


    // Start is called before the first frame update
    void Start()
    {
        binbags = new List<GameObject>();
    }

    IEnumerator waiter()
    {
        // Wait for 10 seconds
        // yield return new WaitForSecondsRealtime(10);
        yield return new WaitForSeconds(10);
    }

    /**
     * Enumerator generating x bin bags on position (x,y) in space
     * @param binBagNb - is the number of bin bags desired
     * @param positiZ - is the z coordinate in space where bags appear
     * @param positiX - is the x coordinate in space where bags appear
     * @param timeBetweenSpawn - is the time between two bags spawn
     */
    private IEnumerator BinBagGenerator(int binBagNb, float positionZ, float positionX, float timeBetweenSpawn)
    {
        Transform scaler = binPrefab.GetComponent<Transform>();
        Rigidbody rigidB = binPrefab.GetComponent<Rigidbody>();

        for (var i = 0; i < binBagNb; i++)
        {
            Vector3 bagPosition = new Vector3(positionX, height, positionZ);
            GameObject binBagGameObject = Instantiate(binPrefab, bagPosition, UnityEngine.Random.rotation);

            Transform binBagGameObjectTransform = binBagGameObject.transform;
            binBagGameObjectTransform.localScale = scaler.transform.localScale;

            binBagGameObject.name = "bag" + i;

            binBagGameObject.SetActive(true);

            binbags.Add(binBagGameObject);

            yield return new WaitForSeconds(timeBetweenSpawn);
        }
    }

    /**
     * Calls a enumerator which will make spawn x bags on (x,z) pos with a frequency of n seconds
     */
    private void GenerateTrash(int binBagNb, float positionZ, float positionX, float timeBetweenSpawn)

    {
        positionX = position_x;
        positionZ = position_z;
        StartCoroutine(BinBagGenerator(binBagNb, positionZ, positionX, timeBetweenSpawn));
        generated = true;
    }

    public void LaunchSimpleGeneratorScript()
    {
        myResetScript.ResetTrashBags();
        GenerateTrash(
            instanceCount,
            position_z,
            position_x,
            defaultSpawnFrequency);
    }
}