using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.WebCam;

public class WindowsPhotoCapture : MonoBehaviour
{
    private UnityEngine.Windows.WebCam.PhotoCapture photoCaptureObject = null;
    private bool                                    _verbose           = false;
    private string                                  _photoname         = "Default";
    public  bool                                    PhotoProcessing { get; private set; }


    private void Start()
    {
        PhotoProcessing = false;
    }

    /**
     * Public function called by external script in order to launch photo process
     */
    public void BeginPhotoProcess(string photoName)
    {
        _photoname = photoName;
        // If holograms should appear on the photo, first argument must be true
        UnityEngine.Windows.WebCam.PhotoCapture.CreateAsync(true, OnPhotoCaptureCreated);
    }

    private void OnPhotoCaptureCreated(UnityEngine.Windows.WebCam.PhotoCapture captureObject)
    {
        PhotoProcessing = true;
        
        photoCaptureObject = captureObject;

        Resolution cameraResolution =
            UnityEngine.Windows.WebCam.PhotoCapture.SupportedResolutions
                .OrderByDescending((res) => res.width * res.height).First();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 1.0f;
        c.cameraResolutionWidth = cameraResolution.width;
        c.cameraResolutionHeight = cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.BGRA32;

        captureObject.StartPhotoModeAsync(c, OnPhotoModeStarted);
    }

    private void OnPhotoModeStarted(UnityEngine.Windows.WebCam.PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            // Change here for the filename you want
            string filename = string.Format(_photoname);

            // File path is a persistent path only accessible through the Windows Device Portal
            // It usually looks to C:/Data/Users/%UserName/AppData/Local/Packages/Template3D_pzq3xp76mxafg/LocalState
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);

            if (_verbose)
            {
                // Display some information in debug
                Debug.Log("File path is " + Application.persistentDataPath);
            }

            // Taking photo
            photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);
        }
        else
        {
            Debug.LogError("Unable to start photo mode!");
        }
    }

    void OnCapturedPhotoToDisk(UnityEngine.Windows.WebCam.PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            Debug.Log("Saved Photo to disk!");
            photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
        }
        else
        {
            Debug.Log("Failed to save Photo to disk");
        }
    }

    public void OnStoppedPhotoMode(UnityEngine.Windows.WebCam.PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;

        PhotoProcessing = false;
    }

    private void RaiseProcessingFlag(PhotoCapture.PhotoCaptureResult result)
    {
        Debug.Log("Raising the flag");
        PhotoProcessing = true;
    }

    private void DropProcessingFlag(PhotoCapture.PhotoCaptureResult result)
    {
        Debug.Log("Droping the flag");
        PhotoProcessing = false; 
    }

    public bool PhotoCaptureObjectExists()
    {
        
        return photoCaptureObject != null;
    }
} 