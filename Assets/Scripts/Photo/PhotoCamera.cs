using System;
using UnityEngine;


public class PhotoCamera : MonoBehaviour
{
    public WindowsPhotoCapture windowsCaptionProcess = null;

    private void Start()
    {
    }

    public void MakeAPhoto(string photoName)
    {
        windowsCaptionProcess.BeginPhotoProcess(photoName);
    }

    public bool IsPhotoProcessActivated()
    {
        return windowsCaptionProcess.PhotoProcessing;
    }
    
}