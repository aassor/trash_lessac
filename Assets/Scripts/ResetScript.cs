using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ResetScript : MonoBehaviour
{
    public GameObject WeekTrashBags = null;

    public GameObject      YearTrashBagsGO = null;
    public GeneratorScript YearTrashBags   = null;

    public GameObject                       KeyboardTrashBagsGO = null;
    public GeneratorScriptWithKeyboardInput KeyboardTrashBags   = null;

    public GameObject                     PhotoTrashBagsGO = null;
    public GeneratorScriptWithScreenshots PhotoTrashBags   = null;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ResetTrashBags()
    {
        WeekTrashBags.SetActive(false);

        foreach (GameObject bag in YearTrashBags.binbags)
        {
            Destroy(bag);
        }

        
        
        foreach (GameObject bag in KeyboardTrashBags.binbags)
        {
            Destroy(bag);
        }

        
        foreach (GameObject bag in PhotoTrashBags.binbags)
        {
            Destroy(bag);
        }

    }
}