using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using TMPro;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using System;
using UnityEngine.UI;

public class ReadInput : MonoBehaviour
{
    public string                      input;
    public TextMeshPro                 PreviewText;
    public InputField                  field;
    public MixedRealityKeyboardPreview keyboard_txt;
    public TouchScreenKeyboard         keyboard;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ReadStringPhysicalInput();
        keyboard_txt.PreviewText.text = input;
    }

    public void ReadStringPhysicalInput()
    {
        input = keyboard.text;
    }

    public void EnterPhysicalString()
    {
        keyboard = TouchScreenKeyboard.Open("");
        field.ActivateInputField();
        keyboard_txt.enabled = true;
    }
}