using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Microsoft.MixedReality.Toolkit.Experimental.UI;

public class GeneratorScriptWithKeyboardInput : MonoBehaviour
{
    public int                         instanceCount;
    public List<GameObject>            binbags;
    public GameObject                  proto;
    public MixedRealityKeyboardPreview keyboard;
    public GameObject                  physical_keyboard;
    public float                       height;
    public bool                        generated = false;

    public int number;

    public float position_z;

    public float position_x;
    
    public ResetScript myResetScript = null;


    // Start is called before the first frame update
    void Start()
    {
        binbags = new List<GameObject>();
    }

    public void LaunchGeneration()
    {
        GenerateTrash();
    }

    IEnumerator Generator(int instanceCount, float position_z, float position_x)
    {
        Transform scaler = proto.GetComponent<Transform>();
        Rigidbody rigidB = proto.GetComponent<Rigidbody>();
        if (instanceCount == 0)
        {
            Debug.Log("pas de sacs");
            yield return new WaitForSeconds(1);
        }
        else
        {
            for (var i = 0; i < instanceCount; i++)
            {
                Vector3 popos = new Vector3(position_x, height, position_z);
                GameObject t = Instantiate(proto, popos, UnityEngine.Random.rotation);

                //Transform t = Instantiate(prefab);
                Transform t_t = t.transform;
                t_t.localScale = scaler.transform.localScale;
                // t_t.localRotation = UnityEngine.Random.rotation;
                // t_t.localPosition = new Vector3(position_x, height, position_z);
                Debug.Log(position_x);
                Debug.Log(position_z);

                t.name = "bag" + i;
                //t.tag = "bag";
                Debug.Log("ici");
                t.SetActive(true);
                Debug.Log("l�");
                binbags.Add(t);
                // Invoke("ActivateTrashBag", 5);

                yield return new WaitForSeconds(0.4f);
            }
        }

        Debug.Log(instanceCount); 
    }

    public void GenerateTrash()

    {
        myResetScript.ResetTrashBags();
        
        Debug.Log(keyboard.GetComponent<ReadInput>().input);
        int instanceCount = 0;
        bool tryingParsing = int.TryParse(keyboard.GetComponent<ReadInput>().input, out instanceCount);

        if (tryingParsing)
        {
            position_x = this.transform.position.x;
            position_z = this.transform.position.z;
            StartCoroutine(Generator(instanceCount, position_z, position_x));
            generated = true;
        }  
        else
        {
            Debug.Log("Wrong string : conversion into int failed");
        }
        
    }
}