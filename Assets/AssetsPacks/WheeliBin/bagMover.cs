﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bagMover : MonoBehaviour {

    public float speed;
    public GameObject colObj;
    private Vector3 start;
    public Vector3 des;
    private float fraction = 0;


    void Start()
    {
        start = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    void Update()
    {
        start = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        
        if (fraction < 1 & des != new Vector3(0f,0f,0f))
        {
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = true;
            fraction += Time.deltaTime * speed;
            transform.position = Vector3.Lerp(start, des, fraction);
        }
        else
        {
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().isKinematic = false;
        }


 
    }

    void OnCollisionStay(Collision colObj)
    {
        if (colObj.gameObject.tag == "Ground")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            GetComponent<Rigidbody>().angularVelocity= new Vector3(0,0,0);
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }
    }
}
